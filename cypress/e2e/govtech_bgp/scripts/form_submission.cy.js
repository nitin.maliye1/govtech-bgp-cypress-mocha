/// <reference types="cypress" />

const { EligibilityPage } = require("../../../support/page_objects/EligibilityPage");
const { GrantPage } = require("../../../support/page_objects/GrantPage");
const { HomePage } = require("../../../support/page_objects/HomePage");
const { LoginPage } = require("../../../support/page_objects/LoginPage");
const { ContactDetailsPage } = require("../../../support/page_objects/ContactDetailsPage");
const { BusinessImpactPage } = require("../../../support/page_objects/BusinessImpactPage");
const { CostPage } = require("../../../support/page_objects/CostPage");
const { PraposalPage } = require("../../../support/page_objects/PraposalPage");
const { DeclareAndReviewPage } = require("../../../support/page_objects/DeclareAndReviewPage");
const { FormReviewPage } = require("../../../support/page_objects/FormReviewPage");



const env = Cypress.env()
const  homePage = new HomePage();
const loginPage = new LoginPage();
const grantPage = new GrantPage();
const eligibilityPage = new EligibilityPage();
const contactDetailsPage = new ContactDetailsPage();
const businessImpactPage = new BusinessImpactPage();
const costPage = new CostPage();
const praposalPage = new PraposalPage();
const declareAndReviewPage = new DeclareAndReviewPage();
const formReviewPage = new FormReviewPage()

// Welcome to Cypress! change it
//
// This spec file contains a variety of sample tests
// for a GOVTECH bgp app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

// This suite contains automated e2e tests for Form Submission
// As an Applicant, after filling up all mandatory fields in all form sections, I should be able to review 
// and then submit my Grant Application.

describe('US-02-Form-Submission', () => {
  
  beforeEach(() => {
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
  })
    cy.loadBasicAuth(env['bgp_uat_domain'], env['username'], env['password'] );
    homePage.clickLoginOnHomePage()
    loginPage.enterLoginDetails('BGPQEDEMO', 'S1234567A', 'Acceptor', 'Tan Ah Kow')
    cy.url().should('include', 'dashboard')
    cy.get(grantPage.get_new_grant_button, { timeout: 20000 }).should('be.visible')

    grantPage.selectGetNewGrant()
    .selectSectrorIT()
    .selectInterExpanssion()
    .selectMarketRediness()
    .clickApply()
    .clickProceed()
  })

  it.only('Form-Submission-Review', () => {
    
    // AC 1: Upon filling all mandatory inputs in all 6 form sections and clicking the ‘Review’ button in the 
    // ‘Declare and Review’ section, the Applicant should be taken to a read-only summary page.

    //Select  options for all 5 questions and save eligibility form in draft mode.
    eligibilityPage.check_sg_registered_check_true()
    .check_eligibility_turnover_check_true()
    .check_global_hq_check_true()
    .check_new_target_market_check_true()
    .check_started_project_check_true()
    .clickSave()
    .clickNext()

    cy.get(eligibilityPage.success_message).should('be.visible')

    //Add contact details
    contactDetailsPage.enterName("Nitin")
    .enterDesignation("SDET")
    .enterPhone("66775555")
    .enterPrimaryEmail("nitin.maliye@gmail.com")
    .checkCopyAddress()
    .checkCopyContactInfo()
    .clickSave()
    .clickNext()

    cy.get(contactDetailsPage.success_message).should('be.visible')
    
    //Add Praposal Details
    praposalPage.enterProjectTitle("BGP automation project")
    .enterProjectStartDate("25 Jun 2022")
    .enterProjectEndDate("20 Jun 2023")
    .enterProjectDescription("This is test automation project")
    .selectProjectActivity("FTA Consultancy")
    .selectProjectPrimaryMarket("India")
    .checkFirstTimeExpansionTrue()
    .clickSave()
    .clickNext()

    cy.get(praposalPage.success_message).should('be.visible')

    //Add Business impact details
    businessImpactPage.enterProjectImpactFyEnd("25 Dec 2022")
    .enterImpactOverseasSale0("1000000")
    .enterImpactOverseasSale1("2000000")
    .enterImpactOverseasSale2("3000000")
    .enterImpactOverseasSale3("4000000")
    .enterImpactOverseasInvestment0("100000")
    .enterImpactOverseasInvestment1("200000")
    .enterImpactOverseasInvestment2("300000")
    .enterImpactOverseasInvestment3("400000")
    .enterProjectImpactRationaleRemarks("Good Quality. Big Opportunities.")
    .enterProjectImpactBenefitsRemarks("Tech Transfer")
    .clickSave()
    .clickNext()

    cy.get(businessImpactPage.success_message).should('be.visible')

    //Add Cost details
    costPage.SelectLocalVendorTrue()
    .selectVendor("DYNAMIC TESTING")
    .uploadVendorDocuments('vendor_cost.png')//https://github.com/abramenal/cypress-file-upload/issues/355
    .enterEstimatedCost("10000")
    .clickSave()
    .clickNext()

    cy.get(costPage.success_message).should('be.visible')

    //Fill declaration form and review
    declareAndReviewPage.checkCriminalLiability()
    .checkCivilProceeding()
    .checkInsolvencyProceeding()
    .checkProjectIncentives()
    .checkOtherIncentives()
    .checkProjectCommence()
    .checkRelatedParty()
    .checkDebartment()
    .checkCovidSafe()
    .checkCovidSafeQuestion()
    .checkConcentAck()
    .clickReview()


    //Verify that after clicking on review button, summary page is displayed.
    cy.url().should('include', 'view')

    //Submit form
    cy.get(formReviewPage.declaration_info_truthfulness_check, { timeout: 20000 }).should('be.visible')
    formReviewPage.checkTruthfullness()
    .clickSubmit()

    //Verify form details
    cy.get(formReviewPage.form_details, { timeout: 20000 }).should('be.visible')
    //ALL DONE.
    
  })

  it('Form-Submission-Validation-Errors', () => {
    // AC 2: If there are any mandatory inputs missing, a validation error should trigger and the form 
    // should redirect to the section with the missing details. An error number should also be shown in the 
    // sidebar next to the offending section.

    //Verify that validation error message displayed on missing fields.
    //Verify that missing details section displayed.
    //Verify that error number should be displayed in sidebar.
  })

  it('Form-Submission-Summary-Details', () => {
    // AC 3: The read-only summary page should correctly contain all the details previously filled in each 
    // form section.
    // AC 4: At the bottom of the read-only summary page is a final ‘Consent and Acknowledgement’ 
    // heckbox.

    //Verify summary details.
    //Verify ‘Consent and Acknowledgement’ check box is displayed.

  })

  it('Form-Submission', () => {
    // AC 5: Once checked, the Applicant can submit the entire Application and a Success message box 
    // should be shown. The Success message box should contain an Application Ref ID and Agency details 
    // should display the receiving Agency as ‘Enterprise Singapore’.

    // AC 6: Upon submission, the main ‘My Grants’ dashboard should show the Application under the 
    // ‘Processing’ tab

    //Verify that form is submitted successfully after checking ‘Consent and Acknowledgement’.

    //Verify success message box details

    //Go to My Grants and verify that submitted application is displayed under Processing Tab.

  })

  afterEach(() => {
    cy.log("this is teardown")
    
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
  })

});