/// <reference types="cypress" />

const { GrantPage } = require("../../../support/page_objects/GrantPage");
const { HomePage } = require("../../../support/page_objects/HomePage");
const { LoginPage } = require("../../../support/page_objects/LoginPage");



const env = Cypress.env()
const  homePage = new HomePage();
const loginPage = new LoginPage();
const grantPage = new GrantPage();

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a GOVTECH bgp app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

//This suite contains automated e2e tests for Contact details

describe('US-02-Contact Details', () => {
  
  beforeEach(() => {
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
  })
    cy.loadBasicAuth(env['bgp_uat_domain'], env['username'], env['password'] );
    homePage.clickLoginOnHomePage()
    loginPage.enterLoginDetails('BGPQEDEMO', 'S1234567A', 'Acceptor', 'Tan Ah Kow')

    //Go to Contact details page.
    //TODO
  })

  it('Contact-Details-Main-contact-person', () => {
    
    // AC 1: The page contains a ‘Main Contact Person’ subsection with the following inputs:
    //  Name
    //  Job Title
    //  Contact No
    //  Email
    //  Alternate Contact Person’s Email
    //  Mailing Address
    // AC 5: There should be an option ‘ Same as main contact person’ which will populate the subsection

    //Verify all the Main contact person details.

    //Verify ‘ Same as main contact person’ option.
    
  })

  it('Mailing-Address-Auto-Complete', () => {
    // AC 2: The Mailing Address input should be able to take in a valid postal code which auto-populates
    // the ‘Blk/Hse No.’ and ‘Street details’ from an external API (One-map).

    //Verify that On entering valid postal code, Blk/Hse No and Street Details populated automatically.
  })

  it('Mailing-Address-Same-As-Reg-Address', () => {
    // AC 3: Alternatively, there should be a checkbox ‘Same as registered address in Company Profile’
    // which will auto-populate Mailing Address details from the Applicant’s Company Profile.

    //Verify that on checking ‘Same as registered address in Company Profile’ Mailing address details
    //gets populated from applicants company profile

  })

  it('Letter-Of-Offer-Adressee-Details', () => {
    // AC 4: The page also contains a ‘Letter of Offer Addressee’ subsection with the following inputs:
    //  Name
    //  Job Title
    //  Email
    // AC 5: There should be an option ‘ Same as main contact person’ which will populate the subsection

    //Verify ‘Letter of Offer Addressee’ Section details

    //Verify ‘ Same as main contact person’ option.

  })

  it('Save-Contact-Details', () => {
    // AC 6: Clicking ‘Save’ will save the Applicant’s inputs and refreshing the page should reload the saved
    // values.

    //Verify all contact details are saved sucessfully

  })

  it('Contact-Details=Page-refresh', () => {
    // AC 6: Clicking ‘Save’ will save the Applicant’s inputs and refreshing the page should reload the saved
    // values.

    //Verify all contact details are loaded successfullay after page refresh.

  })

  afterEach(() => {
    cy.log("this is teardown")
    
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
  })

});