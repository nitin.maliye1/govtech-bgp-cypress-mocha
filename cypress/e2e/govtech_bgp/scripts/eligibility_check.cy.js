/// <reference types="cypress" />

const { AuthenticationPage } = require("../../../support/page_objects/AuthenticationPage");
const { EligibilityPage } = require("../../../support/page_objects/EligibilityPage");
const { GrantPage } = require("../../../support/page_objects/GrantPage");
const { HomePage } = require("../../../support/page_objects/HomePage");
const { LoginPage } = require("../../../support/page_objects/LoginPage");



const env = Cypress.env()
const authenticationPage = new AuthenticationPage();
const  homePage = new HomePage();
const loginPage = new LoginPage();
const grantPage = new GrantPage();
const eligibilityPage = new EligibilityPage();

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a GOVTECH bgp app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

//This suite contains automated e2e tests for Eligibility section

describe('US-01-Eligibility Section', () => {
  
  beforeEach(() => {
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
  })
    cy.loadBasicAuth(env['bgp_uat_domain'], env['username'], env['password'] );
    homePage.clickLoginOnHomePage()
    loginPage.enterLoginDetails('BGPQEDEMO', 'S1234567A', 'Acceptor', 'Tan Ah Kow')
    cy.url().should('include', 'dashboard')
    cy.get(grantPage.get_new_grant_button, { timeout: 20000 }).should('be.visible')

    grantPage.selectGetNewGrant()
    .selectSectrorIT()
    .selectInterExpanssion()
    .selectMarketRediness()
    .clickApply()
    .clickProceed()
  })

  it('Eligibility-Save-All-Yes', () => {

    //AC 1: The section should contain 4 questions.
    //AC 2: Each question can be answered Yes or No through radio buttons.

    cy.get(eligibilityPage.sg_registered_check_true).should('be.visible')
    cy.get(eligibilityPage.sg_registered_check_false).should('be.visible')
    cy.log('Ouestion 1 - Is the applicant registered in Singapore? is visible')
    
    cy.get(eligibilityPage.eligibility_turnover_check_true).should('be.visible')
    cy.get(eligibilityPage.eligibility_turnover_check_false).should('be.visible')
    cy.log('Question 2- Is the applicant\'s group sales turnover less than or equal to S$100m or is the applicant\'s' +
    'group employment size less than or equal to 200? is visible')

    cy.get(eligibilityPage.global_hq_check_true).should('be.visible')
    cy.get(eligibilityPage.global_hq_check_false).should('be.visible')
    cy.log('Ouestion 3 - Does the applicant have at least 30% local equity? is visible')

    cy.get(eligibilityPage.new_target_market_check_true).should('be.visible')
    cy.get(eligibilityPage.new_target_market_check_false).should('be.visible')
    cy.log('Ouestion 4 - Is the target market that you are applying for a new market? is visible')

    cy.get(eligibilityPage.started_project_check_true).should('be.visible')
    cy.get(eligibilityPage.started_project_check_false).should('be.visible')
    cy.log('Ouestion 5 - Are all the following statements true for this project? is visible')

    //Select option for all 5 questions and save eligibility form in draft mode.
    eligibilityPage.check_sg_registered_check_true()
    .check_eligibility_turnover_check_true()
    .check_global_hq_check_true()
    .check_new_target_market_check_true()
    .check_started_project_check_true()
    .clickSave()

    cy.log('Selected Yes option for all 5 questions. and Click on Save button.')

    //Verify if eligibility form is saved successfully.
    cy.get(eligibilityPage.success_message).should('be.visible')
    
  })

  it('Eligibility-Save-All-No-Verify-error-message', () => {
    //AC 2: Each question can be answered Yes or No through radio buttons.
    //AC 3: Answering No for any of the questions should display a warning message ‘Visit Smart Advisor 
    //on the SME Portal for more information on other government assistance.’

    eligibilityPage.check_sg_registered_check_false()
    cy.xpath(eligibilityPage.sg_registered_check_warning).should('be.visible')
    cy.log('Question 1 - warning message is visible')

    eligibilityPage.check_eligibility_turnover_check_flase()
    cy.xpath(eligibilityPage.eligibility_turnover_check_warning).should('be.visible')
    cy.log('Question 2 - warning message is visible')

    eligibilityPage.check_global_hq_check_false()
    cy.xpath(eligibilityPage.global_hq_check_warning).should('be.visible')
    cy.log('Question 3 - warning message is visible')

    eligibilityPage.check_new_target_market_check_false()
    cy.xpath(eligibilityPage.new_target_market_check_warning).should('be.visible')
    cy.log('Question 4 - warning message is visible')

    eligibilityPage.check_started_project_check_false()
    cy.xpath(eligibilityPage.started_project_check_warning).should('be.visible')
    cy.log('Question 5 - warning message is visible')

    eligibilityPage.clickSave()

    cy.log('Selected No option for all 5 questions. and Click on Save button.')

    cy.get(eligibilityPage.success_message).should('be.visible')
  })

  it('Eligibility-page-refresh', () => {
    //AC 5: Clicking ‘Save’ will save the Applicant’s inputs and refreshing the page should reload the saved values.

    //Select mixed options for all 5 questions and save eligibility form in draft mode.
    eligibilityPage.check_sg_registered_check_true()
    .check_eligibility_turnover_check_flase()
    .check_global_hq_check_true()
    .check_new_target_market_check_false()
    .check_started_project_check_true()
    .clickSave()

    cy.log('Selected options for all 5 questions. and Click on Save button.')

    //Verify if eligibility form is saved successfully.
    cy.get(eligibilityPage.success_message).should('be.visible')

    //Refresh the page
    cy.reload()

    //Veify if all the selected options are as it is and saved draft can be recalled after page refresh.
    cy.get(eligibilityPage.sg_registered_check_true).should('be.checked')
    cy.get(eligibilityPage.eligibility_turnover_check_false).should('be.checked')
    cy.get(eligibilityPage.global_hq_check_true).should('be.checked')
    cy.get(eligibilityPage.new_target_market_check_false).should('be.checked')
    cy.get(eligibilityPage.started_project_check_true).should('be.checked')

    cy.log('all the selected options are as it is and saved draft can be recalled after page refresh.')
  })

  it('Eligibility-FAQ', () => {
    //AC 4: Clicking the link in the warning message in AC 3 will launch a website in another window tab to 
    //the url: https://www.smeportal.sg/content/smeportal/en/moneymatters.html#saText

    eligibilityPage.check_sg_registered_check_false()
    cy.xpath(eligibilityPage.sg_registered_check_warning).should('be.visible')
    cy.log('Question 1 - warning message is visible')

    //Click on the FAQ link and verify if it is opned in new tab
    eligibilityPage.clickFAQLink()
    cy.url().should('include', 'www.gobusiness.gov.sg/business-grants-portal-faq/get-a-grant/')

  })

  afterEach(() => {
    cy.log("this is teardown")
    
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
  })

});