/// <reference types="cypress" />
const { xpath } = require('cypress-xpath')

export class ContactDetailsPage {
    // All the locators used from Contact Details Page are enlisted below
    contact_info_name = '#react-contact_info-name'
    contact_info_designation= '#react-contact_info-designation'
    contact_info_phone= '#react-contact_info-phone'
    contact_info_primary_email = '#react-contact_info-primary_email'
    contact_info_correspondence_address_copied = '#react-contact_info-correspondence_address-copied'
    contact_info_copied= '#react-contact_info-copied'
    save_button = '#save-btn'
    next_button = '#next-btn'
    success_message = '[class="growl growl-notice growl-medium"]'
  
    //All the actions performed on Contact Details Page are defined below
    enterName(name) {
      cy.get(this.contact_info_name).type(name)
      return this
    }

    enterDesignation(designation) {
      cy.get(this.contact_info_designation).type(designation)
      return this
    }
    
    enterPhone(phone) {
      cy.get(this.contact_info_phone).type(phone)
      return this
    }
    
    enterPrimaryEmail(email) {
      cy.get(this.contact_info_primary_email).type(email)
      return this
    }
  
    checkCopyAddress() {
      cy.get(this.contact_info_correspondence_address_copied).click()
      return this
    }
  
    checkCopyContactInfo() {
      cy.get(this.contact_info_copied).click()
      return this
    }
  
    clickSave() {
      cy.get(this.save_button).click()
      return this
    }
  
    clickNext() {
      cy.get(this.next_button).click()
    }
  
    is_success_message_present() {
      return (cy.get(this.success_message).length) > 0
    }
  
  }