/// <reference types="cypress" />
const { xpath } = require('cypress-xpath')

export class EligibilityPage {
    // All the locators used from Eligibility Page are enlisted below
    sg_registered_check_true = '#react-eligibility-sg_registered_check-true'
    sg_registered_check_false= '#react-eligibility-sg_registered_check-false'
    sg_registered_check_warning= '//input[@id=\'react-eligibility-sg_registered_check-true\']/following::div[@class=\'field-warning-text\']'
    eligibility_turnover_check_true = '#react-eligibility-turnover_check-true'
    eligibility_turnover_check_false = '#react-eligibility-turnover_check-false'
    eligibility_turnover_check_warning= '//input[@id=\'react-eligibility-turnover_check-true\']/following::div[@class=\'field-warning-text\']'
    global_hq_check_true = '#react-eligibility-global_hq_check-true'
    global_hq_check_false = '#react-eligibility-global_hq_check-false'
    global_hq_check_warning= '//input[@id=\'react-eligibility-global_hq_check-true\']/following::div[@class=\'field-warning-text\']'
    new_target_market_check_true = '#react-eligibility-new_target_market_check-true'
    new_target_market_check_false = '#react-eligibility-new_target_market_check-false'
    new_target_market_check_warning= '//input[@id=\'react-eligibility-new_target_market_check-true\']/following::div[@class=\'field-warning-text\']'
    started_project_check_true = '#react-eligibility-started_project_check-true'
    started_project_check_false = '#react-eligibility-started_project_check-false'
    started_project_check_warning= '//input[@id=\'react-eligibility-started_project_check-true\']/following::div[@class=\'field-warning-text\']'
    save_button = '#save-btn'
    next_button = '#next-btn'
    faq_link = '//div[@class=\'field-warning-text\']//a[text()=\'FAQ\']'
    success_message = '[class="growl growl-notice growl-medium"]'
  
    //All the actions performed on Eligibility Page are defined below
  
    check_sg_registered_check_true() {
      cy.get(this.sg_registered_check_true).click()
      return this
    }
  
    check_sg_registered_check_false() {
      cy.get(this.sg_registered_check_false).click()
      return this
    }
  
    is_sg_registered_check_warning_present() {
      return (cy.find(cy.xpath(this.sg_registered_check_warning)).length) > 0
    }
  
    check_eligibility_turnover_check_true() {
      cy.get(this.eligibility_turnover_check_true).click()
      return this
    }
  
    check_eligibility_turnover_check_flase() {
      cy.get(this.eligibility_turnover_check_false).click()
      return this
    }
  
    is_eligibility_turnover_check_warning_present() {
      return (cy.find(cy.xpath(this.eligibility_turnover_check_warning)).length) > 0
    }
  
    check_global_hq_check_true() {
      cy.get(this.global_hq_check_true).click()
      return this
    }
  
    check_global_hq_check_false() {
      cy.get(this.global_hq_check_false).click()
      return this
    }
  
    is_global_hq_check_warning_present() {
      return (cy.find(cy.xpath(this.global_hq_check_warning)).length) > 0
    }
  
    check_new_target_market_check_true() {
      cy.get(this.new_target_market_check_true).click()
      return this
    }
  
    check_new_target_market_check_false() {
      cy.get(this.new_target_market_check_false).click()
      return this
    }
  
    is_new_target_market_check_warning_present() {
      return (cy.find(cy.xpath(this.new_target_market_check_warning)).length) > 0
    }
  
    check_started_project_check_true() {
      cy.get(this.started_project_check_true).click()
      return this
    }
  
    check_started_project_check_false() {
      cy.get(this.started_project_check_false).click()
      return this
    }
  
    is_started_project_check_warning_present() {
      return (cy.find(cy.xpath(this.started_project_check_warning)).length) > 0
    }
  
    clickSave() {
      cy.get(this.save_button).click()
      return this
    }
  
    clickNext() {
      cy.get(this.next_button).click({force:true})
    }
  
    clickFAQLink() {
      //cy.xpath(this.faq_link).invoke('removeAttr', 'target').click()
      cy.xpath(this.faq_link).invoke('removeAttr', 'target').click()
    }
  
    is_success_message_present() {
      return (cy.get(this.success_message).length) > 0
    }
  
  }