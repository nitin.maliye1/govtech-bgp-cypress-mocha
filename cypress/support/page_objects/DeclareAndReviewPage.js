/// <reference types="cypress" />
const { xpath } = require('cypress-xpath')

export class DeclareAndReviewPage {
    // All the locators used from Declare and review Page are enlisted below
    declaration_criminal_liability_check_false = '#react-declaration-criminal_liability_check-false'
    declaration_civil_proceeding_check_false= '#react-declaration-civil_proceeding_check-false'
    declaration_insolvency_proceeding_check_false= '#react-declaration-insolvency_proceeding_check-false'
    declaration_project_incentives_check_false= '#react-declaration-project_incentives_check-false'
    declaration_other_incentives_check_false= '#react-declaration-other_incentives_check-false'
    declaration_project_commence_check_false= '#react-declaration-project_commence_check-false'
    declaration_related_party_check_false= '#react-declaration-related_party_check-false'
    declaration_debarment_check_false= '#react-declaration-debarment_check-false'
    declaration_covid_safe_check_true= '#react-declaration-covid_safe_check-true'
    declaration_covid_safe_ques_check_true = '#react-declaration-covid_safe_ques_check-true'
    declaration_consent_acknowledgement_check = '#react-declaration-consent_acknowledgement_check'
    save_button = '#save-btn'
    review_button = '#review-btn'
    success_message = '[class="growl growl-notice growl-medium"]'
  
    //All the actions performed on declare and review Page are defined below
  
    checkCriminalLiability() {
      cy.get(this.declaration_criminal_liability_check_false).click()
      return this
    }
  
    checkCivilProceeding() {
      cy.get(this.declaration_civil_proceeding_check_false).click()
      return this
    }
  
    checkInsolvencyProceeding() {
      cy.get(this.declaration_insolvency_proceeding_check_false).click()
      return this
    }
  
    checkProjectIncentives() {
      cy.get(this.declaration_project_incentives_check_false).click()
      return this
    }
  
    checkOtherIncentives() {
      cy.get(this.declaration_other_incentives_check_false).click()
      return this
    }
  
    checkProjectCommence() {
      cy.get(this.declaration_project_commence_check_false).click()
      return this
    }
  
    checkRelatedParty() {
      cy.get(this.declaration_related_party_check_false).click()
      return this
    }
  
    checkDebartment() {
      cy.get(this.declaration_debarment_check_false).click()
      return this
    }
  
    checkCovidSafe() {
      cy.get(this.declaration_covid_safe_check_true).click()
      return this
    }
  
    checkCovidSafeQuestion() {
      cy.get(this.declaration_covid_safe_ques_check_true).click()
      return this
    }
  
    checkConcentAck() {
      cy.get(this.declaration_consent_acknowledgement_check).click()
      return this
    }
  
    clickSave() {
      cy.get(this.save_button).click()
      return this
    }
  
    clickReview() {
      cy.get(this.review_button).click()
    }
  
    is_success_message_present() {
      return (cy.get(this.success_message).length) > 0
    }
  
  }