/// <reference types="cypress" />
const { xpath } = require('cypress-xpath')

export class FormReviewPage {
    // All the locators used from review Page are enlisted below
    declaration_info_truthfulness_check = '#react-declaration-info_truthfulness_check'
    submit_btn = '#submit-btn'
    success_message = '[class="growl growl-notice growl-medium"]'
    form_details = '[class="card"]'
  
    //All the actions performed on review Page are defined below
  
    checkTruthfullness() {
      cy.get(this.declaration_info_truthfulness_check).click()
      return this
    }

    clickSubmit() {
      cy.get(this.submit_btn).click()
    }
  
    is_success_message_present() {
      return (cy.get(this.success_message).length) > 0
    }
  
  }