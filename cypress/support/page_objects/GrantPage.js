/// <reference types="cypress" />

import { EligibilityPage } from "./EligibilityPage"

export class GrantPage {
    // All the locators used from Login Page are enlisted below
    get_new_grant_button = '#dashboard-menubox-app-apply-grant'
    sector_it_button= '#IT'
    inter_expansion_button = '[id="International Expansion"]'
    market_redinnes_button = '[id="Market Readiness Assistance 2"]'
    go_to_grant_button = '#go-to-grant'
    proceed_button = '#keyPage-form-button'
  
    //All the actions performed on Login Page are defined below
  
    selectGetNewGrant() {
      cy.get(this.get_new_grant_button).click({force: true})
      return this
    }
  
    selectSectrorIT() {
      cy.get(this.sector_it_button).click({force: true})
      return this
    }
  
    selectInterExpanssion() {
      cy.get(this.inter_expansion_button).click({force: true})
      return this
    }
  
    selectMarketRediness() {
      cy.get(this.market_redinnes_button).click({force: true})
      return this
    }
  
    clickApply() {
      cy.get(this.go_to_grant_button).click({force: true})
      return this
    }
  
    clickProceed() {
      cy.get(this.proceed_button).click({force: true})
      return new EligibilityPage()
    }
  
  }