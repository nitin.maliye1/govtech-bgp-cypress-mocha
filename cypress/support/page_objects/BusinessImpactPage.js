/// <reference types="cypress" />
const { xpath } = require('cypress-xpath')

export class BusinessImpactPage {
    // All the locators used from Business Impact Page are enlisted below
    project_impact_fy_end_date = '#react-project_impact-fy_end_date_0'
    project_impact_overseas_sales_0= '#react-project_impact-overseas_sales_0'
    project_impact_overseas_sales_1= '#react-project_impact-overseas_sales_1'
    project_impact_overseas_sales_2= '#react-project_impact-overseas_sales_2'
    project_impact_overseas_sales_3= '#react-project_impact-overseas_sales_3'
    project_impact_overseas_investments_0= '#react-project_impact-overseas_investments_0'
    project_impact_overseas_investments_1= '#react-project_impact-overseas_investments_1'
    project_impact_overseas_investments_2= '#react-project_impact-overseas_investments_2'
    project_impact_overseas_investments_3= '#react-project_impact-overseas_investments_3'
    project_impact_rationale_remarks = '#react-project_impact-rationale_remarks'
    project_impact_benefits_remarks = '#react-project_impact-benefits_remarks'
    save_button = '#save-btn'
    next_button = '#next-btn'
    success_message = '[class="growl growl-notice growl-medium"]'
  
    //All the actions performed on Business Impact Page are defined below
    enterProjectImpactFyEnd(endDate) {
      cy.get(this.project_impact_fy_end_date).type(endDate)
      return this
    }

    enterImpactOverseasSale0(amount) {
      cy.get(this.project_impact_overseas_sales_0).type(amount)
      return this
    }

    enterImpactOverseasSale1(amount) {
      cy.get(this.project_impact_overseas_sales_1).type(amount)
      return this
    }

    enterImpactOverseasSale2(amount) {
      cy.get(this.project_impact_overseas_sales_2).type(amount)
      return this
    }

    enterImpactOverseasSale3(amount) {
      cy.get(this.project_impact_overseas_sales_3).type(amount)
      return this
    }
    
    enterImpactOverseasInvestment0(amount) {
      cy.get(this.project_impact_overseas_investments_0).type(amount)
      return this
    }
    
    enterImpactOverseasInvestment1(amount) {
      cy.get(this.project_impact_overseas_investments_1).type(amount)
      return this
    }
    
    enterImpactOverseasInvestment2(amount) {
      cy.get(this.project_impact_overseas_investments_2).type(amount)
      return this
    }
    
    enterImpactOverseasInvestment3(amount) {
      cy.get(this.project_impact_overseas_investments_3).type(amount)
      return this
    }
    
    enterProjectImpactRationaleRemarks(remarks) {
      cy.get(this.project_impact_rationale_remarks).type(remarks)
      return this
    }
    
    enterProjectImpactBenefitsRemarks(remarks) {
      cy.get(this.project_impact_benefits_remarks).type(remarks)
      return this
    }
  
    clickSave() {
      cy.get(this.save_button).click()
      return this
    }
  
    clickNext() {
      cy.get(this.next_button).click()
    }
  
    is_success_message_present() {
      return (cy.get(this.success_message).length) > 0
    }
  
  }