/// <reference types="cypress" />

export class AuthenticationPage {
    // All the locators used from Authentication Page are enlisted below
    
    userName = '#signInFormUsername'
    password = '#signInFormPassword'
    login_button = 'input[name="signInSubmitButton"]'
  
    //All the actions performed on Authentication Page are defined below
    enterUserName(userName) {
      cy.get(this.userName).type(userName)
    }
  
    enterPassword(password) {
      cy.get(this.password).type(password)
    }
  
    clickLogin() {
      cy.get(this.login_button).click()
    }
  
    enterAuthDetails(userName, password) {
      this.enterUserName(userName)
      this.enterPassword(password)
      this.clickLogin()
    }
  
  }