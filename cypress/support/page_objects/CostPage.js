/// <reference types="cypress" />
const { xpath } = require('cypress-xpath')

export class CostPage {
    // All the locators used from Cost Page are enlisted below
    project_cost_endors_accordion_header = '#react-project_cost-vendors-accordion-header'
    project_cost_vendors_add_item= '#react-project_cost-vendors-add-item'
    project_cost_local_vendor_true= '#react-project_cost-vendors-0-local_vendor-true'
    project_cost_vendor_name= '#react-project_cost-vendors-0-vendor_name'
    select_project_cost_vendor_name_vendor= '#react-project_cost-vendors-0-vendor_name-vendor'
    slected_vendor_name= '//span[@id=\'vendor-row-sub\'][contains(text(),\'%s1\')]'
    project_cost_vendors_attachments_btn= '#react-project_cost-vendors-0-attachments-btn'
    project_estimated_cost= '#react-project_cost-vendors-0-amount_in_billing_currency'
    project_cost_vendors_remarks= '#react-project_cost-vendors-0-remarks'
    save_button = '#save-btn'
    next_button = '#next-btn'
    success_message = '[class="growl growl-notice growl-medium"]'
  
    //All the actions performed on Cost Page are defined below

    formatString (...args) {
      const str = args[0];
      const params = args.filter((arg, index) => index !== 0);
      if (!str) return "";
      return str.replace(/%s[0-9]+/g, matchedStr => {
        const variableIndex = matchedStr.replace("%s", "") - 1;
        return params[variableIndex];
      });
    }

    SelectLocalVendorTrue() {
      cy.get(this.project_cost_endors_accordion_header).click()
      cy.get(this.project_cost_vendors_add_item).click()
      cy.get(this.project_cost_local_vendor_true).click()
      return this
    }

    selectVendor(vendorName) {
      cy.get(this.project_cost_vendor_name).type(vendorName)
      cy.get(this.select_project_cost_vendor_name_vendor).click()
      cy.get('#vendor-row-sub')
      .should('contain', vendorName).click()
      //cy.xpath(this.formatString(this.vendorName, vendorName)).click()
      return this
    }

    uploadVendorDocuments(documentPath) {
      cy.get(this.project_cost_vendors_attachments_btn).attachFile(documentPath, {
        force: true,
        subjectType: 'drag-n-drop'
    })
      return this
    }

    enterEstimatedCost(amount) {
      cy.get(this.project_estimated_cost).type(amount)
      return this
    }

    enterRemarks(remarks) {
      cy.get(this.project_cost_vendors_remarks).type(remarks)
      return this
    }
  
    clickSave() {
      cy.get(this.save_button).click()
      return this
    }
  
    clickNext() {
      cy.get(this.next_button).click()
    }
  
    is_success_message_present() {
      return (cy.get(this.success_message).length) > 0
    }
  
  }