/// <reference types="cypress" />
const { xpath } = require('cypress-xpath')

export class PraposalPage {
    // All the locators used from Praposal Page are enlisted below
    project_title = '#react-project-title'
    project_start_date= '#react-project-start_date'
    poject_end_date= '#react-project-end_date'
    project_description = '#react-project-description'
    select_project_activity = '#react-select-project-activity--value'
    select_project_activity_DDL = '.Select-menu-outer'
    select_project_primary_market = '#react-select-project-primary_market--value'
    project_is_first_time_expand_true = '#react-project-is_first_time_expand-true'
    save_button = '#save-btn'
    next_button = '#next-btn'
    success_message = '[class="growl growl-notice growl-medium"]'
  
    //All the actions performed on Praposal Page are defined below
    enterProjectTitle(projectTitle) {
      cy.get(this.project_title).type(projectTitle)
      return this
    }

    enterProjectStartDate(startDate) {
      cy.get(this.project_start_date).type(startDate)
      return this
    }
    
    enterProjectEndDate(endDate) {
      cy.get(this.poject_end_date).type(endDate)
      return this
    }
    
    enterProjectDescription(description) {
      cy.get(this.project_description).type(description)
      return this
    }
  
    selectProjectActivity(activity) {
      cy.xpath('(//div[@class=\'Select-placeholder\'])[1]').click({force:true})
      cy.get(this.select_project_activity_DDL)
        .children('*')
        .should('contain', 'FTA').click()
      return this
    }
  
    selectProjectPrimaryMarket(market) {
      cy.xpath('(//div[@class=\'Select-placeholder\'])[1]').click({force:true})
      cy.get(this.select_project_activity_DDL)
        .children('*')
        .should('contain', 'India').click()
      return this
    }
  
    checkFirstTimeExpansionTrue() {
      cy.get(this.project_is_first_time_expand_true).click()
      return this
    }
  
    clickSave() {
      cy.get(this.save_button).click()
      return this
    }
  
    clickNext() {
      cy.get(this.next_button).click()
    }
  
    is_success_message_present() {
      return (cy.get(this.success_message).length) > 0
    }
  
  }