# GovTech-BGP-Cypress-Mocha



## Project Overview:
This project is created to design and develop automation framework to automate Govtech BGP UI test cases using Cypress.

## ------------ Getting started -----------:

## Software requirements:

### OS:
    Windows 7 and above
    macOS 10.9 and above(64 bit only)
    Linux Ubuntu 12.04 and above, Fedora 21 and Debian 8 (64 bit only)

### Softwares:
    Node.js 10 or 12 and above

## Project Setup:
Please follow below steps to do the initial setup.

    ### clone this repository to a local directory
    https://gitlab.com/nitin.maliye1/govtech-bgp-cypress-mocha

    ### cd into the cloned repo
    cd govtech-bgp-cypress-mocha

    ### install the node_modules
    npm install



## Test Execution:

    ### Local test execution using Cypress Application:
    Open cypress application using "npx cypress open"
    Launch E2E Testing
    To launch test execution click on any script.

    ### Local test execution using Cypress Run:
    npm run cy:test

## Reporting:

    ### To generate reports please follow below steps
    npm run cy:generate-report

    ### To view reports
    npm run cy:view-report

## CI-CD:

    ### CI pipeline is created with below 3 Jobs:
        1. Build - Triggers on all branch check-in
        2. Test - Triggers on all only main check-in
        3. Report (.post) - Triggers on only branch check-in
    
    To view pipeline execution please visit - https://gitlab.com/nitin.maliye1/govtech-bgp-cypress-mocha/-/pipelines

## Visuals
    ###Videos are recorded and are placed under `cypress >> videos`
    https://gitlab.com/nitin.maliye1/govtech-bgp-cypress-mocha/-/tree/main/cypress/videos

    ###The screenshots are taken on failure and recorded under `cypress >> screenshots`
    https://gitlab.com/nitin.maliye1/govtech-bgp-cypress-mocha/-/tree/main/cypress/screenshots

## Roadmap
    1. Integrate screenshots and videos to allure reports.
    2. BDD support.

## License
    ISC

## Project status
Only US01- Eligibility criteria related e2e test cases are automated.

## Known Issues
    1. Only chrome browser is supported as there application redirects url to different domain and this is not supported by other browsers using cypress.
    2. BGP application logs out while launching dashboard after login. This issue not exist if test cases executed using cypress application. CI pipeline gets failed due to this issue.
    3. Screenshots created with large file names which is not supported on CI pipeline.
